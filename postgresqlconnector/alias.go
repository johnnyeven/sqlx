package postgresqlconnector

import "gitee.com/go-genie/sqlx/connectors/postgresql"

type PostgreSQLConnector = postgresql.PostgreSQLConnector
