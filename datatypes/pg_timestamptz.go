package datatypes

import (
	"database/sql/driver"
	"fmt"
	"time"
)

var (
	TimestampTzZero     = PgTimestampTz(time.Time{})
	TimestampTzUnixZero = PgTimestampTz(time.Unix(0, 0))
)

type PgTimestampTz time.Time

func (p *PgTimestampTz) UnmarshalText(text []byte) (err error) {
	str := string(text)
	if len(str) == 0 || str == "0" {
		return nil
	}
	*p, err = ParseTimestampTzFromString(str)
	return
}

func (p *PgTimestampTz) MarshalText() (text []byte, err error) {
	return []byte(p.String()), nil
}

func (p *PgTimestampTz) String() string {
	if p.IsZero() {
		return ""
	}
	return time.Time(*p).In(CST).Format(time.RFC3339)
}

func (p *PgTimestampTz) Unix() int64 {
	return time.Time(*p).Unix()
}

func (p *PgTimestampTz) IsZero() bool {
	unix := p.Unix()
	return unix == 0 || unix == TimestampZero.Unix()
}

func (p *PgTimestampTz) Value() (driver.Value, error) {
	return p.String(), nil
}

func (p *PgTimestampTz) Scan(value any) error {
	switch v := value.(type) {
	case time.Time:
		*p = PgTimestampTz(v)
	case nil:
		*p = TimestampTzZero
	default:
		return fmt.Errorf("cannot sql.Scan() strfmt.Timestamp from: %#v", v)
	}
	return nil
}

func (p *PgTimestampTz) DataType(driverName string) string {
	return "timestamptz"
}

func ParseTimestampTzFromString(s string) (dt PgTimestampTz, err error) {
	var t time.Time
	t, err = time.Parse(time.RFC3339, s)
	dt = PgTimestampTz(t)
	return
}
