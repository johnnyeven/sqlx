package er_test

import (
	"encoding/json"

	"gitee.com/go-genie/sqlx/er"
	"gitee.com/go-genie/sqlx/generator/__examples__/database"
	"gitee.com/go-genie/sqlx/postgresqlconnector"
)

func ExampleDatabaseERFromDB() {
	ers := er.DatabaseERFromDB(database.DBTest, &postgresqlconnector.PostgreSQLConnector{})
	_, _ = json.MarshalIndent(ers, "", "  ")
	// Output:
}
