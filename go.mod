module gitee.com/go-genie/sqlx

go 1.21

require (
	gitee.com/go-genie/codegen v1.0.1
	gitee.com/go-genie/enumeration v1.0.1
	gitee.com/go-genie/logr v1.0.1
	gitee.com/go-genie/metax v1.0.1
	gitee.com/go-genie/packagesx v1.0.1
	gitee.com/go-genie/xx v1.0.1
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0
	github.com/lib/pq v1.10.3
	github.com/onsi/gomega v1.18.1
	github.com/pkg/errors v0.9.1
)

require (
	golang.org/x/mod v0.6.0-dev.0.20220106191415-9b9b3d81d5e3 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sys v0.0.0-20220315194320-039c03cc5b86 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.10 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
